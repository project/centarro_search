<?php

namespace Drupal\centarro_search\Plugin\search_api\backend;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Plugin\search_api\data_type\value\TextValueInterface;
use Drupal\search_api\Query\ConditionGroupInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\Query\ResultSet;
use Drupal\search_api\SearchApiException;
use Elastic\EnterpriseSearch\AppSearch\Request\CreateEngine;
use Elastic\EnterpriseSearch\AppSearch\Request\DeleteDocuments;
use Elastic\EnterpriseSearch\AppSearch\Request\GetEngine;
use Elastic\EnterpriseSearch\AppSearch\Request\IndexDocuments;
use Elastic\EnterpriseSearch\AppSearch\Request\ListDocuments;
use Elastic\EnterpriseSearch\AppSearch\Request\PutSchema;
use Elastic\EnterpriseSearch\AppSearch\Request\Search;
use Elastic\EnterpriseSearch\AppSearch\Schema\Engine;
use Elastic\EnterpriseSearch\AppSearch\Schema\PaginationResponseObject;
use Elastic\EnterpriseSearch\AppSearch\Schema\SchemaUpdateRequest;
use Elastic\EnterpriseSearch\AppSearch\Schema\SearchRequestParams;
use Elastic\EnterpriseSearch\AppSearch\Schema\SimpleObject;
use Elastic\EnterpriseSearch\Client;

/**
 * Elastic Enterprise Search backend for search api.
 *
 * @SearchApiBackend(
 *   id = "centarro_search_ees",
 *   label = @Translation("Elastic Enterprise Search (Centarro Search)"),
 *   description = @Translation("Index items using an Elastic Enterprise search server.")
 * )
 */
class ElasticEnterpriseSearchBackend extends BackendPluginBase implements PluginFormInterface {

  use PluginFormTrait {
    submitConfigurationForm as traitSubmitConfigurationForm;
  }

  /**
   * The Elastic client.
   *
   * @var \Elastic\Elasticsearch\Client
   */
  protected $client;

  /**
   * The Elastic Enterprise client.
   *
   * @var \Elastic\EnterpriseSearch\Client
   */
  protected $enterpriseClient;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'app_search_host' => '',
      'app_search_api_key' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    $documents = [];
    /** @var \Drupal\search_api\Item\ItemInterface[] $items */
    foreach ($items as $id => $item) {
      $document = [
        'id' => $id,
        'search_api_language' => $item->getLanguage(),
        'search_api_datasource' => $item->getDatasourceId(),
      ];
      $fields = $item->getFields();
      foreach ($fields as $field) {
        $values = $field->getValues();
        // @todo check if we should we skip empty values here?
        if (empty($values)) {
          continue;
        }
        $values = array_map(function ($value) use ($field) {
          if ($value instanceof TextValueInterface) {
            return $value->toText();
          }
          // The APP search doesn't support boolean filtering... so we "cheat"
          // and index booleans as integers.
          if (is_bool($value)) {
            return (int) $value;
          }
          // Timestamps aren't accepted as dates, so convert them.
          // @todo check if the format is correct.
          if ($field->getType() === 'date' && is_numeric($value)) {
            return date('c', $value);
          }

          return $value;
        }, $values);
        // @todo figure out what to do exactly here (do some processing?).
        $document += [
          $field->getFieldIdentifier() => count($values) === 1 ? reset($values) : $values,
        ];
      }
      $documents[] = $document;
    }
    try {
      $client = $this->getEnterpriseClient();
      $index_documents_request = new IndexDocuments($this->getEngineName($index), $documents);
      $response = $client->appSearch()->indexDocuments($index_documents_request)->asArray();
      $indexed_ids = [];
      // Holds the document IDS that failed to be indexed.
      $failed_ids = [];
      foreach ($response as $document) {
        if (!empty($document['errors'])) {
          $failed_ids[] = $document['id'];
          continue;
        }
        $indexed_ids[] = $document['id'];
      }
      // @todo check if we should fail or just log the error here?
      // In case the indexing partially succeeded, it might make sense to
      // return the IDS of items that were successfully indexed.
      if (!empty($failed_ids)) {
        throw new SearchApiException(sprintf('Cannot index the following ids, check the ES APP Search logs: (%s).', implode(', ', $failed_ids)));
      }

      return $indexed_ids;
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    try {
      $client = $this->getEnterpriseClient();
      $delete_documents_request = new DeleteDocuments($this->getEngineName($index), array_values($item_ids));
      $client->appSearch()->deleteDocuments($delete_documents_request);
    }
    catch (\Exception $exception) {
      throw new SearchApiException($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    $current_page = 1;
    $engine_name = $this->getEngineName($index);
    // Unfortunately, there is no method for deleting all documents, so we
    // have to list the documents and deleting them by ID.
    while (TRUE) {
      try {
        $client = $this->getEnterpriseClient();
        $list_documents_request = new ListDocuments($engine_name);
        $list_documents_request->setPageSize(1000);
        $list_documents_request->setCurrentPage($current_page);
        $response = $client->appSearch()->listDocuments($list_documents_request)->asArray();
        if (empty($response['results'])) {
          break;
        }
        $document_ids = array_map(function ($doc) {
          return $doc['id'];
        }, $response['results']);
        $delete_documents_request = new DeleteDocuments($engine_name, $document_ids);
        $client->appSearch()->deleteDocuments($delete_documents_request);
        if ($response['meta']['page']['current'] === $response['meta']['page']['total_pages']) {
          break;
        }
        $current_page++;
      }
      catch (\Exception $exception) {
        throw new SearchApiException($exception->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    $original_query = clone $query;
    $response = $this->doSearch($query);
    $index = $query->getIndex();
    $results = $query->getResults();

    if (empty($response['results'])) {
      return;
    }

    $results->setResultCount($response['meta']['page']['total_results']);
    foreach ($response['results'] as $result) {
      if (!isset($result['_meta']['id'])) {
        continue;
      }
      $result_item = $this->fieldsHelper->createItem($index, $result['_meta']['id']);
      $result_item->setScore($result['_meta']['score']);
      foreach ($result as $name => $field_value) {
        if ($name === '_meta' || !isset($field_value['raw'])) {
          continue;
        }
        $field = $this->fieldsHelper->createField($index, $name);
        $values = !is_array($field_value['raw']) ? [$field_value['raw']] : $field_value['raw'];
        $field->setValues($values);
        $result_item->setField($name, $field);
      }
      $results->addResultItem($result_item);
    }

    // Handle the facets result when enabled.
    if ($facets = $query->getOption('search_api_facets')) {
      $or_facets = array_filter($facets, function ($facet) {
        return $facet['operator'] === 'or';
      });
      // When there are "OR" search facets, we need to perform an additional
      // query for each of the "OR" facet in which we remove the current
      // facet from the query filters.
      if ($or_facets) {
        foreach ($or_facets as $name => $facet) {
          $facet_query = clone $original_query;
          // Fake the "search_api_facets" option so that only the current facet
          // is handled in parseFacets().
          $facet_query->setOption('search_api_facets', array_intersect_key($facets, [$name => $name]));
          // For each of the "OR" facet, we need to perform a query that has
          // all the filters, besides the current facet.
          $facet_query->setOption('filters_to_skip', [$name]);
          $facet_query_response = $this->doSearch($facet_query);
          $this->parseFacets($results, $facet_query, $facet_query_response);
        }
      }
      // Extract the remaining facets that aren't "OR" facets, note that we
      // can use the original query for that.
      if (count($or_facets) !== count($facets)) {
        $original_query->setOption('search_api_facets', array_diff_key($facets, $or_facets));
        $this->parseFacets($results, $original_query, $response);
      }
    }
  }

  /**
   * Executes a search on this server.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The query to execute.
   *
   * @return array
   *   The search response.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error prevented the search from completing.
   */
  protected function doSearch(QueryInterface $query) {
    $filters = $this->buildFilters($query->getConditionGroup(), $query);
    if ($query->getLanguages()) {
      if (isset($filters['all'])) {
        $filters['all'][]['search_api_language'] = $query->getLanguages();
      }
      else {
        $filters['search_api_language'] = $query->getLanguages();
      }
    }
    $search_query = $query->getKeys() ?? '';
    $search = new SearchRequestParams($search_query);
    // @todo check if filters & facets are correctly passed.
    if ($filters) {
      $filters_object = new SimpleObject();
      foreach ($filters as $key => $filter) {
        $filters_object->$key = $filter;
      }
      $search->filters = $filters_object;
    }
    $facets = $this->buildFacets($query);
    if ($facets) {
      $facets_object = new SimpleObject();
      foreach ($facets as $key => $facet) {
        $facets_object->$key = $facet;
      }
      $search->facets = $facets_object;
    }
    $limit = $query->getOption('limit');
    $offset = $query->getOption('offset');

    $pagination = new PaginationResponseObject();
    // Handle pagination.
    if ($limit) {
      $pagination->size = $limit;
    }
    if ($offset) {
      // The "current" page starts at 1 for the first page, therefore we need
      // to add 1.
      $pagination->current = (int) (ceil($offset / $limit) + 1);
    }
    $search->page = $pagination;
    // @todo this needs to handle fields that can't be sorted on.
    foreach ($query->getSorts() as $field => $order) {
      // Relevance is a special field.
      $field = $field === 'search_api_relevance' ? '_score' : $field;
      $search->sort[] = [$field => strtolower($order)];
    }
    $index = $query->getIndex();
    try {
      $client = $this->getEnterpriseClient();
      // Perform the search query.
      $request = $client->appSearch()->search(
        new Search(
          $this->getEngineName($index),
          $search,
        )
      );

      return $request->asArray();
    }
    catch (\Exception $exception) {
      $this->getLogger()->error($exception->getMessage());
      throw new SearchApiException($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['app_search_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('APP Search Host'),
      '#default_value' => $this->configuration['app_search_host'],
      '#required' => TRUE,
    ];
    $form['app_search_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('APP Search API Key'),
      '#default_value' => $this->configuration['app_search_api_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->traitSubmitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedFeatures() {
    return [
      'search_api_facets',
      'search_api_facets_operator_or',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex(IndexInterface $index) {
    try {
      $this->ensureEngine($index);
    }
    catch (\Exception $exception) {
      $this->getLogger()->error($exception->getMessage());
    }
    try {
      $client = $this->getEnterpriseClient();
      $schema_update_request =new SchemaUpdateRequest();
      foreach ($this->buildSchema($index) as $name => $type) {
        $schema_update_request->{$name} = $type;
      }
      $schema_request = new PutSchema($this->getEngineName($index), $schema_update_request);
      $client->appSearch()->putSchema($schema_request);
    }
    catch (\Exception $exception) {
      $this->getLogger()->error($exception->getMessage());
    }
    if ($this->indexFieldsUpdated($index)) {
      $index->reindex();
    }
  }

  /**
   * Recursively build the search "filters".
   *
   * @param \Drupal\search_api\Query\ConditionGroupInterface $condition_group
   *   The group of conditions.
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The query.
   *
   * @return array
   *   The search filters.
   */
  protected function buildFilters(ConditionGroupInterface $condition_group, QueryInterface $query) {
    $conjuction_mapping = [
      'AND' => 'all',
      'OR' => 'any',
    ];
    if (!isset($conjuction_mapping[$condition_group->getConjunction()])) {
      return [];
    }
    $filters_to_skip = $query->getOption('filters_to_skip', []);
    $operator_mapping = [
      '<>' => 'none',
      'NOT IN' => 'none',
      'NOT BETWEEN' => 'none',
    ];
    $index = $query->getIndex();
    $field_types = [];
    // Builds an array holding the field type for each field.
    foreach ($index->getFields() as $name => $field) {
      $field_types[$name] = $field->getType();
    }
    $filter_group = $conjuction_mapping[$condition_group->getConjunction()];
    $filters = [];
    // Build the filters.
    foreach ($condition_group->getConditions() as $condition) {
      if ($condition instanceof ConditionGroupInterface) {
        $subfilters = $this->buildFilters($condition, $query);
        if (!empty($subfilters)) {
          $filters[$filter_group][] = $subfilters;
        }
        continue;
      }
      $field = $condition->getField();
      // Skip the "OR" facets, if specified in the query options.
      if (!empty($filters_to_skip) &&
        in_array($field, $filters_to_skip, TRUE)) {
        continue;
      }
      $operator = $condition->getOperator();
      $field_type = $field_types[$field] ?? NULL;
      // Check if the filter should belong to a "subgroup".
      $filter_subgroup = $operator_mapping[$operator] ?? NULL;
      $value = $condition->getValue();
      $original_value = $value;
      // We do this to pass the values to array_map, so we don't duplicate
      // code for scalar values and values that already come as an array
      // for "BETWEEN" and "NOT BETWEEN" operators.
      if (is_scalar($value)) {
        $value = [$value];
      }
      $value = array_map(function ($value) use ($field_type) {
        if ($field_type === 'date' && is_numeric($value)) {
          return date('c', $value);
        }
        elseif ($field_type === 'integer') {
          $value = (int) $value;
        }
        elseif ($field_type === 'decimal') {
          $value = (float) $value;
        }

        return $value;
      }, $value);
      // No need to use an array for the filter itself.
      if (is_scalar($original_value)) {
        $value = reset($value);
      }
      // Handle date/number filters as "ranges".
      if (in_array($operator, ['<', '>', '<=', '>='], TRUE) &&
        in_array($field_type, ['date', 'integer', 'decimal'], TRUE)) {
        if ($operator === '>=' || $operator === '>') {
          // Because the "from" operator is inclusive, add 1 to fake the
          // greater than operator.
          if ($operator === '>' && $field_type === 'integer') {
            $value += 1;
          }
          $filters[$filter_group][][$field] = [
            'from' => $value,
          ];
        }
        else {
          // Because the "to" operator is exclusive, add 1 to fake the
          // lower than or equal operator.
          if ($operator === '<=' && $field_type === 'integer') {
            $value += 1;
          }
          // Try to determine if we are adding a "to" clause to an existing
          // filter or not.
          if (isset($filters[$filter_group])) {
            $count_filters = count($filters[$filter_group]);
            $filter_key = (key($filters[$filter_group][$count_filters - 1]) === $field) ? $count_filters - 1 : $count_filters;
          }
          else {
            $filter_key = 0;
          }
          $filters[$filter_group][$filter_key][$field]['to'] = $value;
        }
        continue;
      }
      if ($operator === 'BETWEEN' || $operator === 'NOT BETWEEN') {
        $value = [
          'from' => $value[0],
          'to' => $value[1],
        ];
      }
      // "<>" operators should have their own filter group.
      if ($filter_subgroup) {
        $filters[$filter_group][$filter_subgroup][][$field] = $value;
      }
      else {
        $filters[$filter_group][][$field] = $value;
      }
    }

    return $filters;
  }

  /**
   * Helper method for creating the "facets" query parameters.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The Search API query.
   *
   * @return array
   *   The "facets" query parameters.
   */
  protected function buildFacets(QueryInterface $query): array {
    $facets = $query->getOption('search_api_facets', []);
    if (empty($facets)) {
      return [];
    }
    // @todo Support more facets (ranges?).
    $facet_type_mapping = [
      'search_api_string' => 'value',
      'search_api_date' => 'value',
    ];
    $facet_params = [];
    foreach ($facets as $facet) {
      if (!isset($facet_type_mapping[$facet['query_type']])) {
        continue;
      }
      $facet_params[$facet['field']] = [
        'type' => $facet_type_mapping[$facet['query_type']],
        // 250 is the maximum limit, as specified in the Elastic APP search
        // documentation.
        'size' => 250,
      ];
    }

    return $facet_params;
  }

  /**
   * Gets an instantiated Elastic Enterprise client.
   *
   * @return \Elastic\EnterpriseSearch\Client
   *   The Enterprise search client.
   */
  protected function getEnterpriseClient() {
    if (isset($this->enterpriseClient)) {
      return $this->enterpriseClient;
    }
    $configuration = [
      'host' => $this->configuration['app_search_host'],
      'app-search' => [
        'apiKey' => $this->configuration['app_search_api_key'],
      ],
    ];
    $client = new Client($configuration);
    $this->enterpriseClient = $client;
    return $this->enterpriseClient;
  }

  /**
   * Ensures an engine exists for the given index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The Search API index.
   *
   * @return string
   *   The engine name.
   */
  protected function ensureEngine(IndexInterface $index) {
    $client = $this->getEnterpriseClient();
    $engine_name = $this->getEngineName($index);
    try {
      $client->appSearch()->getEngine(
        new GetEngine($engine_name)
      );
    }
    catch (\Exception $exception) {
      $this->getLogger()->error($exception->getMessage());
      $engine = new Engine($engine_name);
      $engine->type = 'default';
      $client->appSearch()->createEngine(new CreateEngine($engine));
    }

    return $engine_name;
  }

  /**
   * Gets the engine name for the given index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The Search API index.
   *
   * @return string
   *   The engine name.
   */
  protected function getEngineName(IndexInterface $index): string {
    $third_party_settings = $index->getThirdPartySettings('centarro_search');
    if (!empty($third_party_settings['engine'])) {
      return $third_party_settings['engine'];
    }

    // @todo remove this fallback?
    return str_replace('_', '-', $index->id());
  }

  /**
   * Parse the result set and add the facet values.
   *
   * @param \Drupal\search_api\Query\ResultSet $results
   *   Result set, all items matched in a search.
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   Search API query object.
   * @param array $response
   *   The search response.
   */
  protected function parseFacets(ResultSet $results, QueryInterface $query, array $response) {
    if (!isset($response['facets'])) {
      return;
    }
    $facets = $query->getOption('search_api_facets');

    $field_types = [];
    foreach ($query->getIndex()->getFields() as $name => $field) {
      $field_types[$name] = $field->getType();
    }
    // Because this method is called from within a loop, reuse the FACETS
    // already stored in the results extra data so we don't override
    // already processed facets.
    $attach = $results->getExtraData('search_api_facets', []);
    foreach ($response['facets'] as $name => $data) {
      if (!isset($facets[$name])) {
        continue;
      }
      $terms = [];
      $field_type = $field_types[$name] ?? NULL;
      foreach ($data[0]['data'] as $value) {
        // Convert date strings to timestamps to allow date processing with
        // facets.
        if ($field_type === 'date' &&
          !is_numeric($value['value'])) {
          $value['value'] = strtotime($value['value']);
        }
        $terms[] = [
          'count' => $value['count'],
          'filter' => '"' . $value['value'] . '"',
        ];
      }
      $attach[$name] = $terms;
    }

    $results->setExtraData('search_api_facets', $attach);
  }

  /**
   * {@inheritdoc}
   */
  protected function validateOperator($operator) {
    switch ($operator) {
      case '=':
      case '<>':
      case 'BETWEEN':
        return;
    }
    throw new SearchApiException("Unknown operator '$operator' used in search query condition");
  }

  /**
   * Builds the index schema.
   *
   * @todo figure out how to specify the schema.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The Search API index.
   *
   * @return array
   *   The schema.
   */
  protected function buildSchema(IndexInterface $index): array {
    $schema = [];
    $types_mapping = [
      'date' => 'date',
      'decimal' => 'number',
      'integer' => 'number',
      'string' => 'text',
      'text' => 'text',
    ];
    foreach ($index->getFields() as $field) {
      if (!isset($types_mapping[$field->getType()])) {
        continue;
      }
      $schema[$field->getFieldIdentifier()] = $types_mapping[$field->getType()];
    }

    return $schema;
  }

  /**
   * Checks if the recently updated index had any fields changed.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index that was just updated.
   *
   * @return bool
   *   TRUE if any of the fields were updated, FALSE otherwise.
   */
  protected function indexFieldsUpdated(IndexInterface $index): bool {
    // Get the original index, before the update. If it cannot be found, err on
    // the side of caution.
    if (!isset($index->original)) {
      return TRUE;
    }
    /** @var \Drupal\search_api\IndexInterface $original */
    $original = $index->original;

    $old_fields = $original->getFields();
    $new_fields = $index->getFields();
    if (!$old_fields && !$new_fields) {
      return FALSE;
    }
    if (array_diff_key($old_fields, $new_fields) || array_diff_key($new_fields, $old_fields)) {
      return TRUE;
    }

    return FALSE;
  }

}
