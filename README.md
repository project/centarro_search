# Centarro Search

The Centarro Search module offers an implementation of the Search API that uses an Elastic Enterprise server for 
indexing content (version 8.x).

It uses the Elastic Search API for indexing/updating/deleting documents and the Elastic APP Search API for Search. 

For a full description of the module, visit the
[project page](https://www.drupal.org/project/centarro_search).

The module was designed to support Elasticsearch index-based engines.

## Table of contents

- Requirements
- Installation
- Configuration

## Requirements

This module requires the following modules:

- [Search API](https://www.drupal.org/project/search_api)

This module requires the following libraries:

- [elastic/enterprise-search](https://github.com/elastic/enterprise-search-php)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

First, create an "Elastic Enterprise Search (Centarro Search)" Search API server, and an index using it.

On the server configuration page, the following needs to be configured:
- APP Search Host (Found under the APP search credentials)
- APP Search API Key (Found under the APP search credentials)

On the Search API index configuration page, configure the engine and the index to use. Note that the APP search 
engine must be based on an Elastic search index.

When setting up a search API view based of of this index if Fulltext search is added as exposed filter for Parse mode use Direct query.

## Setup of Elasticsearch cloud

Go to [Elasticsearch](https://www.elastic.co/enterprise-search) page and from there you can get access to Elasticsearch Cloud.

- From the Search overview page in the menu on the left side find Enterprise search section and under it find and go to App search. Create an Engine, choose "App search managed docs". Set the engine name (this is used on Drupal side on the index configuration for the Engine field).
- On the App search in menu on the left side find and go to Credentials page. From there find and copy private key and Endpoint. On Drupal side on the search api server configuration paste the private key to APP Search API key field and Endpoint paste to APP search Host field.

Documentation for setting up Elasticsearch can be found [here](https://www.elastic.co/guide/en/app-search/current/elasticsearch-engines-create.html).

Once all steps for setting up Elasticsearch and Configuration steps for Drupal side are done index the data.
Now under the APP Search section on Elasticsearch you will be able to utilise full power of the APP search. Three main parts for influencing search results are:

- [Relevance tuning](https://www.elastic.co/guide/en/app-search/current/relevance-tuning-guide.html)
- [Synonyms](https://www.elastic.co/guide/en/app-search/8.12/synonyms-guide.html)
- [Curations](https://www.elastic.co/guide/en/app-search/8.12/curations-guide.html)

After making any changes on APP Search side, clear caches on Drupal side in order to be sure that those changes are picked up by Drupal.
